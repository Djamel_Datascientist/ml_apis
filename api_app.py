from flask import Flask, request
import pandas as pd
import json

app = Flask(__name__)

@app.route('/')
def hello_word():
    return "Machine learning platform"

@app.route('/predict_diseases', methods=['GET','POST'])
def predict_diseases():
    message = request.args.get("message")
    return message

@app.route('/predict_temperatur', methods=['GET','POST'])
def predict_temperatur():
    message = request.args.get("message")
    return message

@app.route('/predict_humidity', methods=['GET','POST'])
def predict_humidity():
    message = request.args.get("message")
    return message

@app.route('/', methods=['GET','POST'])
def predict_humidity():

    df_json_dict = json.loads(df.to_json(orient='records'))
    requests.post(url, data=df_json_dict)
    message = request.args.get("message")
    return message

if __name__ == '__main__':
    app.run(debug=True)